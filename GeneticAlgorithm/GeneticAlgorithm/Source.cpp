#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

struct Point {
    int x;
    int y;
};

struct Parameters{
int a;
int b;
int c;
};

int CONTROL_POINTS_ARRAY_SIZE = 100;
int POPULATION_ARRAY_SIZE = 1000;

int STOP_CRITERION = 10;
int MUTATION_VALUE = 1;

int check(Parameters params, Point points[]);
void crossover(Parameters &parent1, Parameters &parent2);
void mutation(Parameters &object);

int main ()
{
srand (time(NULL));

    Point points [CONTROL_POINTS_ARRAY_SIZE]; 

    for(int i=0;i<CONTROL_POINTS_ARRAY_SIZE;i++){
       points[i].x = i - 50;	//rand() % 100;
       points[i].y = 7*points[i].x*points[i].x + 3*points[i].x + 1;        
    }
    
    
    
    Parameters population[POPULATION_ARRAY_SIZE];
for(int i=0;i<POPULATION_ARRAY_SIZE;i++){
       population[i].a = rand() % 20;
  population[i].b = rand() % 20; 
  population[i].c = rand() % 20;        
    }
    
    /*
    cout << "Parent 1" << endl;
    cout << "a: " << population[0].a << endl;
cout << "b: " << population[0].b << endl;
cout << "c: " << population[0].c << endl;
cout << "Parent 2" << endl;
    cout << "a: " << population[1].a << endl;
cout << "b: " << population[1].b << endl;
cout << "c: " << population[1].c << endl << endl;
crossover(population[0],population[1]);
    cout << "Parent 1" << endl;
    cout << "a: " << population[0].a << endl;
cout << "b: " << population[0].b << endl;
cout << "c: " << population[0].c << endl;
cout << "Parent 2" << endl;
    cout << "a: " << population[1].a << endl;
cout << "b: " << population[1].b << endl;
cout << "c: " << population[1].c << endl << endl;	
cout << "Object" << endl;
    cout << "a: " << population[0].a << endl;
cout << "b: " << population[0].b << endl;
cout << "c: " << population[0].c << endl;
    
    mutation(population[0]);
    
    cout << "Object" << endl;
    cout << "a: " << population[0].a << endl;
cout << "b: " << population[0].b << endl;
cout << "c: " << population[0].c << endl;
*/
    int count = 1;
    bool found = false;
    while(!found){
    for(int i=0;i<(POPULATION_ARRAY_SIZE/2);i++){
    int parent1Index = rand() % POPULATION_ARRAY_SIZE;  
    int parent2Index = rand() % POPULATION_ARRAY_SIZE;    	
    crossover(population[parent1Index], population[parent2Index]);
    }
   
    for(int i=0;i<(POPULATION_ARRAY_SIZE/10);i++){
    int objectIndex = rand() % POPULATION_ARRAY_SIZE;  
    mutation(population[objectIndex]);
    }
   
    for(int i=0;i<POPULATION_ARRAY_SIZE;i++){  
  int val = check(population[i], points);         
      if(val<STOP_CRITERION){
found =  true;
cout << "a: " << population[i].a << endl;
   	cout << "b: " << population[i].b << endl;
   	cout << "c: " << population[i].c << endl;
   	cout << "Iteracja: " << count << endl;
      }                     
    }
   
    count++;
    
    }
    
    /*
    
        for(int i=0;i<POPULATION_ARRAY_SIZE;i++){  
int val = check(population[i], points);         
       if(val<STOP_CRITERION){
       	cout << "a: " << population[i].a << endl;
   cout << "b: " << population[i].b << endl;
   cout << "c: " << population[i].c << endl;
   cout << "Wartosc: " << val << endl;
       }                     
    }
    
    population[0].a=6;
    population[0].b=3;
    population[0].c=1;
    cout << points[99].x << endl;
    cout << points[99].y << endl;
    
    
    for(int i=0;i<POPULATION_ARRAY_SIZE;i++){           
       cout << "a: " << population[i].a << endl;
       cout << "b: " << population[i].b << endl; 
  cout << "c: " << population[i].c << endl << endl;                     
    }*/

  return 0;
}





int check(Parameters params, Point points[]){
Point pointsTmp[CONTROL_POINTS_ARRAY_SIZE];
int value = 0;
for(int i=0;i<CONTROL_POINTS_ARRAY_SIZE;i++){
       pointsTmp[i].x = i - 50;	//rand() % 100;
       pointsTmp[i].y = params.a*pointsTmp[i].x*pointsTmp[i].x + params.b*pointsTmp[i].x + params.c;  
  int sub = pointsTmp[i].y - points[i].y;    
       value += ((sub>0) ? sub : -sub);
    }
    
    /*
    cout << "a: " << params.a << endl;
    cout << "b: " << params.b << endl;
    cout << "c: " << params.c << endl;
    cout << "Wartosc: " << value << endl;
*/
return value;
}



void crossover(Parameters &parent1, Parameters &parent2){
int choose = rand() % 2;
int tmp = 0;
switch(choose){
case 0: 
tmp = parent1.a;
parent1.a = parent2.a;
parent2.a = tmp;
break;
case 1: 
tmp = parent1.b;
parent1.b = parent2.b;
parent2.b = tmp;
break;
case 2: 
tmp = parent1.c;
parent1.c = parent2.c;
parent2.c = tmp;
break;
}
}

void mutation(Parameters &object){
int choose = rand() % 5;
switch(choose){
case 0: 
object.a += MUTATION_VALUE;
break;
case 1: 
object.a -= MUTATION_VALUE;
break;
case 2: 
object.b += MUTATION_VALUE;
break;
case 3: 
object.b -= MUTATION_VALUE;
break;
case 4: 
object.c += MUTATION_VALUE;
break;
case 5: 
object.c -= MUTATION_VALUE;
break;
}
}