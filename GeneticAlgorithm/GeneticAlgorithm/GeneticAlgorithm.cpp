// GeneticAlgorithm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include<cmath>
#include<iostream>
#include<stdlib.h>
#include<time.h>
#include <algorithm>

using namespace std;

struct Pairs {
    float valueX;
    float valueY;
};

struct Factors {
    float valueA;
    float valueB;
    float valueC;
    float fitness;
};

void mutation(Factors &factor, float mutationStep);
void crossing(Factors &parent1, Factors &parent2);
void selection(Factors &factor);
void fitnessSeter(Pairs points[], Factors &factor, int pointsCounter);
bool verify(Factors factor);
int rouletteWheel(Factors factor[], int amountOfPop,float summaryOfAllFitnessem);


bool operator<(const Factors& lhs, const Factors& rhs) {
  return lhs.fitness < rhs.fitness;
}

int _tmain(int argc, _TCHAR* argv[])
{
	clock_t c1,c2;
    int const maxAmountOfPopulation = 1000;
    int const pointsCounter = 100;
    float const mutationStep = 0.5f;
    float const mutationMeasure = 0.05f;
    float const crossingMeasure = 0.7f;

    Pairs points[pointsCounter];
    Factors factors[maxAmountOfPopulation];
      
    c1 = clock();
    
	for (int i = 0; i < pointsCounter; i++) {
		points[i].valueX = rand()%300;
        points[i].valueY = 7*points[i].valueX*points[i].valueX + 5*points[i].valueX + 8;
    }

    for(int i = 0; i < maxAmountOfPopulation; i++){
        factors[i].valueA = (float)(rand()%25);
        factors[i].valueB = (float)(rand()%25);
        factors[i].valueC = (float)(rand()%25); 
        factors[i].fitness = 0.0f;    
    }
   
    for(int i = 0; i < maxAmountOfPopulation; i++){
        fitnessSeter(points, factors[i], pointsCounter);  
    }

    bool found = false;
    int generation = 0;
   
	while(!found){
		
		sort(factors, factors + maxAmountOfPopulation);

		for(int i=0; i < maxAmountOfPopulation; i++) {
			found = verify(factors[i]);
			if (found == true) {
				cout<<"\nFactors A: "<<factors[i].valueA<<" B: "<<factors[i].valueB<< " C: "<< factors[i].valueC;
				cout<<"\n\nSolution found in "<<generation<<" generation ";
				break;
			}
		}


		for(int i=0;i<(maxAmountOfPopulation*crossingMeasure);i++){  
			crossing(factors[i], factors[i+2]);
		}

		for(int i=0;i<(maxAmountOfPopulation*mutationMeasure);i++){
			mutation(factors[i],mutationStep);
			mutation(factors[i+2],mutationStep);
		}


		for(int i = 0; i < maxAmountOfPopulation; i++) {
			fitnessSeter(points, factors[i], pointsCounter);
		}

        generation++;
    }
   
    c2 = clock();
    cout<<"and "<<((float)c2-(float)c1)/CLOCKS_PER_SEC<<" seconds."<<endl<<endl;


	system("pause");

    return 0;
}


void crossing(Factors &parent1, Factors &parent2) {
	int choose = rand() % 3;
	float temporaryFactor = 0.0f;
	switch(choose){
		case 0:
			temporaryFactor = parent1.valueA;
			parent1.valueA = parent2.valueA;
			parent2.valueA = temporaryFactor;
			break;
		case 1:
			temporaryFactor = parent1.valueB;
			parent1.valueB = parent2.valueB;
			parent2.valueB = temporaryFactor;
			break;
		case 2:
			temporaryFactor = parent1.valueC;
			parent1.valueC = parent2.valueC;
			parent2.valueC = temporaryFactor;
			break;
	}
}

void mutation(Factors &factor, float mutationStep){
	int choose = rand() % 6;
	switch(choose){
		case 0:
			factor.valueA += mutationStep;
			break;
		case 1:
			factor.valueA -= mutationStep;
			break;
		case 2:
			factor.valueB += mutationStep;
			break;
		case 3:
			factor.valueB -= mutationStep;
			break;
		case 4:
			factor.valueC += mutationStep;
			break;
		case 5:
			factor.valueC -= mutationStep;
			break;
	}
}

bool verify(Factors factor) {
    if(factor.fitness == 0) {
        return true;
    }
    return false;
}

void fitnessSeter(Pairs points[], Factors &factor, int counter) {

    Pairs randomPoints[100];
	factor.fitness = 0;
    for(int i=0;i<counter;i++){
        randomPoints[i].valueX = points[i].valueX;
        randomPoints[i].valueY = factor.valueA*randomPoints[i].valueX*randomPoints[i].valueX + factor.valueB*randomPoints[i].valueX + factor.valueC;  
		factor.fitness += (abs(randomPoints[i].valueY - points[i].valueY));
        //cout<<"\nX is: "<<points[i].valueX<<" Y is: "<<randomPoints[i].valueY<<" and real is "<<points[i].valueY<<" and fitness is: "<<factor.fitness;
    }
}